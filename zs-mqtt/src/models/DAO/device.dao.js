"use strict";

const Device = require("../device.model");
const mqtt = require("../../MQTT/gatewayClient");
const uuidv4 = require('uuid/v4');
var _ = require("lodash");
const chalk = require("chalk");


exports.listDeviceById = async data =>
  await Device.findOne({ _id: data.deviceId });

exports.listDeviceByIeeeAddr = async data =>
  await Device.findOne({ ieeeAddr: data.ieeeAddr });

exports.listDeviceByNwkAddr = async data =>
  await Device.findOne({ nwkAddr: (data.nwkAddr + "") });

exports.listDevicesByEnvironment = async data =>
  await Device.find({ environment: data.environmentId });

exports.listDevices = async data => {
  let devices = await Device.find();
  return devices;
};

exports.createDevice = async data => {
  let testDevice = await Device.findOne({ ieeeAddr: data.ieeeAddr });
  if (!testDevice) {
    var to = await mqtt.getMqttTopic() + "device";
    //console.log(chalk.blue(JSON.stringify(data)));
    var payload = await {
      messageID: uuidv4(),
      timestamp: Date.now(),
      operation: "add",
      device: [
        {
          name: data.modelId,
          ieeeAddr: data.ieeeAddr,
          epList: data.epList,
          nwkAddr: data.nwkAddr,
          profId: data.profId,
          devId: data.devId,
          modelId: data.modelId,
          status: data.status,
          manufId: data.manufId,
          manufName: data.manufName,
          powerSource: data.powerSource,
          type: data.type,
          //environment : device.environment,
          //blockedUsersList : device.blockedUsersList,
          isActive: true,
          inClusterList: data.inClusterList,
          outClusterList: data.outClusterList
        }
      ]
    }

    let device = await Device.create(payload.device);
    await device.save();

    mqtt.mqttPub(to, payload);

    return device;
  } else {
    console.log(chalk.blue("[nedb] device já existe"));
    console.log(chalk.green("[nedb] nwkaddr atualizado. (old:" + testDevice.nwkAddr + ", new:" + data.nwkAddr + ")"));
    testDevice.nwkAddr = data.nwkAddr;

    let res = await Device.findOneAndUpdate({ ieeeAddr: testDevice.ieeeAddr }, { "nwkAddr": testDevice.nwkAddr, "lastUpdate": Date.now() });

    var to = await mqtt.getMqttTopic() + "device/" + res.ieeeAddr + "/status";
    var payload = {
      messageID: uuidv4(),
      timestamp: Date.now(),
      operation: "edit",
      device: [
        {
          name: res.name,
          ieeeAddr: res.ieeeAddr,
          epList: res.epList,
          nwkAddr: res.nwkAddr,
          profId: res.profId,
          devId: res.devId,
          modelId: res.modelId,
          status: res.status,
          manufId: res.manufId,
          manufName: res.manufName,
          powerSource: res.powerSource,
          type: res.type,
          isActive: true,
          inClusterList: res.inClusterList,
          outClusterList: res.outClusterList
        }
      ]
    }


    mqtt.mqttPub(to, payload);

    return { message: "O dispositivo já existe e att o nwkAddr" };
  }
};

exports.updateDevice = async (param, data) => {
  let device = await Device.findOneAndUpdate({ ieeeAddr: param.deviceId }, { "name": data.name });
  device.lastUpdate = Date.now();
  await device.save();

  var dev = await {
    name: device.name,
    ieeeAddr: device.ieeeAddr,
    epList: device.epList,
    nwkAddr: device.nwkAddr,
    profId: device.profId,
    devId: device.devId,
    modelId: device.modelId,
    status: device.status,
    manufId: device.manufId,
    manufName: device.manufName,
    powerSource: device.powerSource,
    type: device.type,
    isActive: device.isActive,
    inClusterList: device.inClusterList,
    outClusterList: device.outClusterList
  }
  return dev;
};

exports.deleteDevice = async param => {
  return await Device.findOneAndDelete({ _id: param.deviceId });
};

exports.deleteDeviceByIeeeAddr = async data => {
  return await Device.findOneAndDelete({ ieeeAddr: data.ieeeAddr });
};

exports.deleteAllDevices = async () => {
  return await Device.deleteMany({});
};

exports.addNewBlockedUser = async (param, data) => {
  let device = await Device.findOne({ _id: param.deviceId });
  device.blockedUsersList.push(data);
  device.lastUpdate = Date.now();
  await device.save();
  return device;
};

exports.removeBlockedUser = async (param, data) => {
  let device = await Device.findOne({ _id: param.deviceId });
  let dado = data;
  let index = _.pullAllBy(device.blockedUsersList, [data], "blockedUserID");
  device.lastUpdate = Date.now();
  await device.save();
  return device;
};