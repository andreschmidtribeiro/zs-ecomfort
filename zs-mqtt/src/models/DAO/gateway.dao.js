"use strict";

const Gateway = require("../gateway.model");


exports.finGateway = async data => {
  let gateways = await Gateway.find();
  return gateways;
};

exports.listGateway = async () => Gateway.find();

exports.createGateway = async data => {
  var gateway = await Gateway.create(data);
  await gateway.save();
  return gateway;
};

exports.updateGateway = async (gatewayId, data) => {
  var gateway = await Gateway.findOneAndUpdate(
    { _id: gatewayId },
    data,
    { multi: false },
    function (err, numReplaced) { }
  );
  gateway.lastUpdate = Date.now();
  await gateway.save();
  return gateway;
};

exports.deleteGateway = async param => {
  return await Gateway.findOneAndDelete({ _id: param.gatewayId });
};
