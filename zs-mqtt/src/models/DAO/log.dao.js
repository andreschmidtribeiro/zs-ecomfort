"use strict";

const Log = require("../log.model");

exports.listLogById = async data =>
  await Log.findOne({ _id: data.logId });

exports.listLogs = async data => {
  let logs = await Log.find();
  return logs;
};

exports.createLog = async data => {
  let log = await Log.create(data);
  await log.save();
  return log;
};

exports.deleteLog = async param => {
  return await Log.findOneAndDelete({ _id: param.logId });
};
