"use strict";

const Environment = require("../environment.model");

exports.listEnvironmentById = async data =>
  await Environment.findOne({ environmentId: data.environmentId });

exports.listEnvironments = async data => {
  let environments = await Environment.find();
  return environments;
};

exports.createEnvironment = async data => {
  let environment = await Environment.create(data);
  await environment.save();

  var env = await {
    name: environment.name,
    environmentId: environment.environmentId,
    devices: environment.devices
  }
  return env;
};

exports.updateEnvironment = async (environmentId, data) => {
  let environment = await Environment.findOneAndUpdate(
    { environmentId: environmentId },
    data
  );

  environment.lastUpdate = Date.now();
  await environment.save();

  var env = await {
    name: environment.name,
    environmentId: environment.environmentId,
    devices: environment.devices
  }
  return env;
};

exports.deleteEnvironment = async environmentId => {
  return await Environment.findOneAndDelete({ environmentId: environmentId });
};

