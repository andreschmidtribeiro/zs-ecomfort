"use strict";


const Rule = require("../rule.model");
const cronjobs = require("../../cronjbos/rulesCron.services");
var _ = require("lodash");
var Datastore = require("nedb"),
  inMemoryDb = new Datastore();

var querryResult;

exports.listRuleById = async data => await Rule.findOne({ ruleId: data.ruleId });

exports.listRules = async data => {
  let rules = await Rule.find();
  await this.refreshRulesReference();
  await inMemoryDb.find({}, function (err, docs) {
    console.log(docs);
  });
  return rules;
};

exports.createRule = async data => {
  let rule = await Rule.create(data);
  await rule.save();
  inMemoryDb.insert(createRuleReference(rule));

  var rul = await {
    ruleId: rule.ruleId,
    name: rule.name,
    inputs: rule.inputs,
    outputs: rule.outputs,
    schedule: rule.schedule,
    isActive: rule.isActive
  }
  return rul;
};

exports.updateRule = async (ruleId, data) => {
  let rule = await Rule.findOneAndUpdate({ ruleId: ruleId }, data);
  rule.lastUpdate = Date.now();
  await rule.save();
  inMemoryDb.insert(createRuleReference(rule));

  var rul = await {
    ruleId: rule.ruleId,
    name: rule.name,
    inputs: rule.inputs,
    outputs: rule.outputs,
    schedule: rule.schedule,
    isActive: rule.isActive
  }
  return rul;
};

exports.deleteRule = async ruleId => {
  await inMemoryDb.remove({ ruleId: ruleId });
  return await Rule.findOneAndDelete({ ruleId: ruleId });
};

exports.runRules = async () => {
  let rules = await Rule.find();
  rules.forEach(item => {
    cronjobs.job(item, "* * * * * *");
  });
};

exports.startRules = async () => {
  cronjobs.startAll();
};

exports.stopRules = async () => {
  cronjobs.stopAll();
};

exports.applyRulesPolitics = async () => {
  cronjobs.applyPolitics();
};

exports.getRulesWithNwkAddrFromReference = async nwkAddr => {
  let ruleRefs = await inMemoryDb.find(
    { nwkAddr: nwkAddr + "" },
    async function (err, ruleReferences) {
      let rules = [];
      //console.log(await ruleReferences);
      await ruleReferences.forEach(async ruleReference => {
        let rule = await Rule.findOne({ ruleId: ruleReference.ruleId });
        rules.push(rule);
        console.log(rules);
      });
    }
  );
  //ruleRefs =  await getQuerryResult();
};

// exports.getRulesReference = async () => {
//   let refe;
//   await inMemoryDb.find({}, function(err, reference){ console.log(reference); });
//   console.log(refe);
//   return refe;
// };

var createRuleReference = rule => {
  let ruleReference = {
    nwkAddr: "",
    ruleId: ""
  };
  rule.inputs.forEach(input => {
    ruleReference.nwkAddr = input.nwkAddr;
    ruleReference.ruleId = rule.ruleId;
  });
  return ruleReference;
};

exports.refreshRulesReference = async () => {
  let rules = await Rule.find();
  await inMemoryDb.remove({}, { multi: true });
  rules.forEach(rule => {
    inMemoryDb.insert(createRuleReference(rule));
  });
};

function getQuerryResult() {
  return querryResult;
}
