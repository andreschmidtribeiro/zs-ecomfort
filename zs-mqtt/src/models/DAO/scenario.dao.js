"use strict";

const Scenario = require("../scenario.model");

exports.listScenarioById = async scenarioId =>
  await Scenario.findOne({ scenarioId: scenarioId });

exports.listScenarios = async data => {
  let scenarios = await Scenario.find();
  return scenarios;
};

exports.createScenario = async data => {
  let scenario = await Scenario.create(data);
  await scenario.save();

  var scen = await {
    name : scenario.name,
    scenarioId: scenario.scenarioId,
    actions: scenario.actions
  }
  return scen;
};

exports.updateScenario = async (scenarioId, data) => {
  let scenario = await Scenario.findOneAndUpdate(
    { scenarioId: scenarioId },
    data
  );
  scenario.lastUpdate = Date.now();
  await scenario.save();
  
  var scen = await {
    name : scenario.name,
    scenarioId: scenario.scenarioId,
    actions: scenario.actions
  }
  return scen;
};

exports.deleteScenario = async scenarioId => {
  return await Scenario.findOneAndDelete({ scenarioId: scenarioId });
};
