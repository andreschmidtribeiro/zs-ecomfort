const Document = require('camo').Document;

class Scenario extends Document {
    constructor() {
        super();

        this.scenarioId = String;
        this.name = String;
        this.actions = [Object];
        this.created = {
            type: Date,
            default: Date.now
        };
        this.lastUpdate = {
            type: Date,
            default: Date.now
        };
    }

    static collectionName() {
        return 'scenarios';
    }
}

var scenario = Scenario;

module.exports = scenario; 