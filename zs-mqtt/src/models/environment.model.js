const Document = require("camo").Document;

class Environment extends Document {
  constructor() {
    super();

    this.environmentId = String;
    this.name = String;
    this.devices = [Object];
    this.created = {
      type: Date,
      default: Date.now
    };
    this.lastUpdate = {
      type: Date,
      default: Date.now
    };
  }

  static collectionName() {
    return "environments";
  }
}

var environment = Environment;

module.exports = environment;
