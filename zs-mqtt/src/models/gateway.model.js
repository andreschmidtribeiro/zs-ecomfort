const Document = require('camo').Document;

class Gateway extends Document {
    constructor() {
        super();

        this.name = String;
        this.ipv4 = Object;
        this.hwVersion = String,
        this.fwVersion = String;
        this.status = String;
        this.serialNumber = String;
        this.created = {
            type: Date,
            default: Date.now
        };
        this.lastUpdate = {
            type: Date,
            default: Date.now
        };
    }

    static collectionName() {
        return 'gateways';
    }

}

var gateway = Gateway;

module.exports = gateway; 
