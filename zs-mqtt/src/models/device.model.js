const Document = require("camo").Document;

class Device extends Document {
  constructor() {
    super();

    this.name = String;
    this.ieeeAddr = String;
    this.epList = [Number];
    this.nwkAddr = String;
    this.profId = String;
    this.devId = String;
    this.modelId = String;
    this.status = String;
    this.manufId = String;
    this.manufName = String;
    this.powerSource = String;
    this.type = String;
    this.environment = {
      type: String,
      default: null
    };
    // this.blockedUsersList = {
    //   type: [Object],
    //   default: null
    // };
    this.isActive = {
      type: Boolean,
      default: true
    };
    this.created = {
      type: Date,
      default: Date.now
    };
    this.lastUpdate = {
      type: Date,
      default: Date.now
    };
    this.inClusterList = [Number];
    this.outClusterList = [Number];
  }

  static collectionName() {
    return "devices";
  }

}

var device = Device;

module.exports = device;
