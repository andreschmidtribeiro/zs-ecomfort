const Document = require('camo').Document;

class Rule extends Document {
    constructor() {
        super();

        this.ruleId  = String;
        this.name = String;
        this.inputs = [Object];
        this.outputs = [Object];
        this.schedule = Object;
        this.isActive = Boolean;
        this.condition = String;
        this.created = {
            type: Date,
            default: Date.now
        };
        this.lastUpdate = {
            type: Date,
            default: Date.now
        };
    }

    static collectionName() {
        return 'rules';
    }
}

var rule = Rule;

module.exports = rule; 
