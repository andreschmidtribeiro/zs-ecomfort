const Document = require("camo").Document;

class Log extends Document {
  constructor() {
    super();

    this.name = String;
    this.description = String;
    this.message = String;
    this.type = String;
    this.created = {
      type: Date,
      default: Date.now
    };
  }

  static collectionName() {
    return "logs";
  }
}

var log = Log;

module.exports = log;
