const CircularJSON = require("circular-json");
const chalk = require("chalk");
const uuidv4 = require("uuid/v4");
const ruleProcessor = require("../services/ruleProcessor");

const mqtt = require("../MQTT/gatewayClient");
const deviceDAO = require("../models/DAO/device.dao");

exports.messageIncoming = (msg, eventType) => {
  msg.endpoints.forEach(endpoint => {
    let nwkAddr = msg.deviceAddr;
    let ieeeAddr = "";
    let cId = msg.data.cid;
    let type = Object.keys(msg.data.data)[0];
    let group = msg.data.data;
    let value = group[type];

    (async () => {
      try {
        if (ieeeAddr == "") {
          var data = {
            nwkAddr: nwkAddr
          };
          var device = await deviceDAO.listDeviceByNwkAddr(data);
          ieeeAddr = await device.ieeeAddr;
        }
      } catch (err) {
        console.log("Erro ao pegar ieee com nwkAddr!");
      }
      if (ieeeAddr != "") {
        sendMessage(
          {
            ieeeAddr: ieeeAddr,
            epId: endpoint.epId,
            cId: cId,
            type: type,
            value: value
          },
          eventType
        );
      }
    })();
  });
};

exports.messageStatus = (msg) => {
  //console.log(CircularJSON.stringify(msg));

  //let ieeeAddr = msg.endpoints.device.ieeeAddr;
  let status = msg.data;

  //console.log(ieeeAddr + " " + status);
};

const sendMessage = async (msg, type) => {
  var topic = await mqtt.getMqttTopic() + "device/" + msg.ieeeAddr + "/event";

  var payload = {
    messageID: uuidv4(),
    timestamp: Date.now(),
    event: msg
  };
  mqtt.mqttPub(topic, payload);
  ruleProcessor.testIfRuleInput(msg);
};
