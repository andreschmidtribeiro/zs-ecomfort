const ZShepherd = require("zs-gateway");
const shepherd = new ZShepherd("/dev/ttyACM0", {
    net: {
        panId: 0x4200,        
    }
});
const readline = require("readline");
const Q = require("q");
const chalk = require("chalk");
const msgDispatcher = require("../shepherd/zs-messageDispatcher");
const mqtt = require('../MQTT/gatewayClient');
const deviceDAO = require("../models/DAO/device.dao");

let init = false;
let permitJoining = false;
let joinTime = 120;
let joinLeft = 0;

console.log(chalk.blue("(Z-SHEPHERD) Initializing..."));

shepherd.start(function (err) {
    if (err) {
        console.log(chalk.red("(Z-SHEPHERD) Shepherd start error!", err));
        return err;
    } else {
        console.log(chalk.blue("(Z-SHEPHERD) Shepherd started!"));
        //gpioControl.setLed(1, 1, 1, 0);
        return "### Shepherd started ###";
    }
});

shepherd.on("ready", function () {
    init = true;

    const Zive = require("zive");
    const epInfo = {
        profId: 260, // 'HA'
        devId: 257, // 'dimmableLight'
        discCmds: []
    };
    const Ziee = require("ziee");
    let ziee = new Ziee();
    let zive = new Zive(epInfo, ziee);
    shepherd.mount(zive, function (e, epId) {
        if (e || epId != 8) {
            console.log(chalk.red("(Z-SHEPHERD) ERROR!!! Local endpoint: " + epId + ", should be 8."));
            //console.log("returned is: " + epId);
            resetShep(1);
            if (e) console.log("(Z-SHEPHERD) error: " + e);
        } else {
            console.log(chalk.blue("(Z-SHEPHERD) Init Server is ready."));
            console.log(chalk.blue("(Z-SHEPHERD) *********"));
            joinShep(90);
        }
    });
});

shepherd.on("permitJoining", function (joinTimeLeft) {
    if (joinTimeLeft === 0) {
        permitJoining = false;
        console.log(chalk.black.bgGreen("(Z-SHEPHERD) [Join disabled]"));
        //gpioControl.setLed(4, 0, 1, 0);
    }
    joinLeft = joinTimeLeft;
});

shepherd.on("ind", msg => {
    switch (msg.type) {
        case "attReport":
            console.log(chalk.green("(Z-SHEPHERD) [Attribute Report]"));

            msgDispatcher.messageIncoming(msg, "attReport");
            break;
        case "devChange":
            console.log(chalk.green("(Z-SHEPHERD) [ Device Change ]"));
            console.log(chalk.gray(CircularJSON.stringify(msg.data)));
            console.log("");
            console.log(msg);

            msgDispatcher.messageIncoming(msg, "devChange");
            break;
        case "devStatus":
            console.log(chalk.green("(Z-SHEPHERD) [Device Status]"));
            console.log(chalk.green("[z-shepherd] " + msg.data));
            console.log("");

            msgDispatcher.messageStatus(msg);

            break;
        case "devIncoming":
            console.log(chalk.inverse("(Z-SHEPHERD) [Device Incoming]"));
            console.log("Device: ", msg.data);
            console.log("");

            var devIncomingMessage = {
                eventType: "incoming",
                modelId: msg.endpoints[0].device.modelId,
                ieeeAddr: msg.endpoints[0].device.ieeeAddr,
                epList: msg.endpoints[0].device.epList,
                type: msg.endpoints[0].device.type,
                status: msg.endpoints[0].device.status
            };

            let simpleDesc = msg.endpoints[0].getSimpleDesc();
            let newDevice = {
                ieeeAddr: msg.endpoints[0].device.ieeeAddr,
                epList: msg.endpoints[0].device.epList,
                nwkAddr: msg.endpoints[0].device.nwkAddr.toString(),
                devId: simpleDesc.devId.toString(),
                profId: simpleDesc.profId.toString(),
                type: msg.endpoints[0].device.type,
                manufId: msg.endpoints[0].device.manufId.toString(),
                manufName: msg.endpoints[0].device.manufName,
                modelId: msg.endpoints[0].device.modelId,
                powerSource: msg.endpoints[0].device.powerSource,
                status: msg.endpoints[0].device.status,
                inClusterList: simpleDesc.inClusterList,
                outClusterList: simpleDesc.outClusterList
            };

            deviceDAO.createDevice(newDevice);

            break;
        case "devLeaving":
            console.log(chalk.inverse("(Z-SHEPHERD) [Device Leaving]"));
            console.log(msg.data);
            console.log("");
            var devLeavingMessage = {
                eventType: "leaving",
                ieeeAddr: msg.data
            };

            //TODO deletar no BD
            break;
        default:
            console.log(JSON.stringify(msg));
            break;
    }
});


function joinShep(joinTime) {
    //gpioControl.setLed(4, 0, 500, joinTime);
    shepherd.permitJoin(Number(joinTime), function (err) {
        if (err) console.log("(Z-SHEPHERD) !!! Join error", err);
        else {
            permitJoining = true;
            console.log(
                chalk.black.bgGreen(
                    "(Z-SHEPHERD) [Permit Join enabled: " + joinTime + " seconds]"
                )
            );
            //gpioControl.setLed(4, 0, 1, 0);
        }
    });
}

function resetShep(mode) {
    console.log(chalk.yellow("(Z-SHEPHERD) Reseting..."));
    var result = 'Reset error!';
    shepherd.reset(Number(mode), function (err) {
        if (!err) {
            console.log(chalk.green("(Z-SHEPHERD) Reset successfully."));
            result = "Reset ok!";
        } else {
            console.log(chalk.red("(Z-SHEPHERD) Reset error."));
            result = "Reset Nok!";
        }
    });
    return result;
}

exports.reset = nJSON => {
    return new Promise((resolve, reject) => {
        shepherd.reset(Number(nJSON.mode), function (err) {
            if(!err) {
                resolve();
            } else {
                reject(err.toString());
            }
            //(err)? resolve() : reject()
        });
    }); 
}

exports.deviceCommand = nJSON => {
    return new Promise((resolve, reject) => {
        shepherd.find(nJSON.ieeeAddr, Number(nJSON.epId))
            .functional(nJSON.cId, nJSON.cmd, nJSON.zclData, function (err, rsp) {
                if (err) reject(err.toString());
                else {
                    //console.log("functional rodou!");
                    resolve(rsp);
                }
            });
    });
};

exports.addDevice = joinTime => {
    return new Promise((resolve, reject) => {
        shepherd.permitJoin(Number(joinTime), function (err) {
            if (err) reject(err.toString());
            else {
                permitJoining = true;
                resolve(joinTime);
            }
            //gpioControl.setLed(4, 0, 1, 0);
        });
    });
};

exports.remove = nJSON => {
    return new Promise((resolve, reject) => {
        shepherd.remove(nJSON.ieeeAddr + "", function (err, rsp) {
            if (err) reject(err.toString())
            else {
                console.log("Dispositivo Removido!");
                resolve(rsp);
            }
        })
    });
};

exports.listAll = nJSON => {
    return new Promise((resolve, reject) => {
        console.log(shepherd.list());
        resolve(shepherd.list());
    });
};

exports.newBind = (interruptor, saida) => {
    return new Promise(function (resolve, reject) {
        let ep1 = shepherd.find(interruptor.ieeeAddr, Number(interruptor.epId));
        let ep2 = shepherd.find(saida.ieeeAddr, Number(saida.epId));

        ep1.bind(interruptor.cId, ep2, function (err) {
            if (err) {
                console.log(chalk.green("[z-shepherd] Bind failed..."));
                reject(err);
            } else {
                console.log(chalk.green("[z-shepherd] Successfully bind: (", interruptor.cId,") ", ep1, "->", ep2));
                resolve({
                    dev1: ep1.device.ieeeAddr,
                    dev2: ep2.device.ieeeAddr,
                    cId: interruptor.cId
                });
            }
        });
    });
}

exports.unbind = (interruptor, saida) => {
    return new Promise(function (resolve, reject) {
        let ep1 = shepherd.find(interruptor.ieeeAddr, Number(interruptor.epId));
        let ep2 = shepherd.find(saida.ieeeAddr, Number(saida.epId));
        //console.log(ep1);
        ep1.unbind(interruptor.cId, ep2, function (err) {
            if(err){
                console.log(chalk.green("[z-shepherd] Unbind failed..."));
                reject(err);
            }else{
                console.log(chalk.green("[z-shepherd] Successfully unbind: (", interruptor.cId,") ", ep1, "->", ep2));
                resolve({
                    dev1: ep1.device.ieeeAddr,
                    dev2: ep2.device.ieeeAddr,
                    cId: interruptor.cId
                });
            }
        });
    });
}

module.exports = {
    shepherd: shepherd
};