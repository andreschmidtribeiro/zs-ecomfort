const zs = require("./zs-shepherd");
const chalk = require("chalk");

exports.addDevice = joinTime => {
    //gpioControl.setLed(4, 0, 500, joinTime);
    return new Promise((resolve, reject) => {
        zs.shepherd.permitJoin(Number(joinTime), function (err) {
            if (err) reject ("(Z-SHEPHERD) !!! Join error: ", err);
            else resolve("(Z-SHEPHERD) [Permit Join enabled: " + joinTime + " seconds]");
        });
    });
};