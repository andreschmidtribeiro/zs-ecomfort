const gatewayDAO = require("../src/models/DAO/gateway.dao");

exports.create = () => {
    var gat = {
        name : "Gateway André",
        ipv4 : {
            address: "192.168.0.100",
            mask: "255.0.0.0",
            gateway: "192.168.0.1"
        },
        hwVersion : "0.0.1",
        hwVersion : "0.0.1",
        status : "active",
        serialNumber : "3b6a-12be-97bc-t9q0-k98e" 
    }
    
    gatewayDAO.createGateway(gat);
    console.log("Gateway Criado!");
}