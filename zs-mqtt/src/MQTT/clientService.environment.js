const zsShepherd = require("../shepherd/zs-shepherd");
const chalk = require("chalk");

const environmentDAO = require("../models/DAO/environment.dao");

exports.execRequest = async (environmentId, operation, nJSON) => {
    return new Promise((resolve, reject) => {
        if (!operation) {
            console.log(chalk.green("[mqtt-service] environment/config"));

            var data = {
                environmentId: nJSON.environmentId,
                name: nJSON.name,
                devices: nJSON.devices
            }
            environmentDAO.createEnvironment(data).then(function (environment){
                console.log(chalk.green("[nedb] environment adicionado."));
                resolve(environment);
            }).catch(function (err) {
                reject(err.toString());
            });
        } else {
            switch (operation) {
                case "edit":
                    console.log(chalk.green("[mqtt-service] environment/+/edit"));

                    var data = {
                        environmentId: environmentId,
                        name: nJSON.name,
                        devices: nJSON.devices
                    }
                    environmentDAO.updateEnvironment(environmentId, data).then(function (environment){
                        console.log(chalk.green("[nedb] Environment Editado."));
                        resolve(environment);
                    }).catch(function (err) {
                        reject(err.toString());
                    });
                    break;
                case "delete":
                    console.log(chalk.green("[mqtt-service] environment/+/delete"));

                    environmentDAO.deleteEnvironment(environmentId).then(function (){
                        console.log(chalk.green("[nedb] environment removido."));
                        resolve("");
                    }).catch(function (err) {
                        reject(err.toString());
                    });
                    break;
            }
        }
    });
}