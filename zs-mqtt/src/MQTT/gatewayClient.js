const mqtt = require("mqtt");
const chalk = require("chalk");
const uuidv4 = require('uuid/v4');
const Gateway = require("../models/gateway.model");
const mqttServices = require("./clientService");

var preTopic = "";

mqttGatewayClient = mqtt.connect("mqtt://191.252.113.160:1883");

mqttGatewayClient.on("connect", connack => {
    if (connack.sessionPresent) {
        console.log(chalk.blue("(MQTT) Cliente já conectado."));
    } else {
        console.log(chalk.blue("(MQTT) Primeira sessão. Assinando tópicos..."));
        Gateway.findOne({ status: "active" }).then(function (gateway) {
            var topic = "ecomfort/iot/v1/s2g/gateway/" + gateway._id;
            preTopic = topic;
            console.log(chalk.blue("(MQTT) topic: ", topic));

            mqttGatewayClient.subscribe(
                [
                    //topic + "/#",
                    topic + "/list",
                    topic + "/reset",
                    topic + "/addDevice",
                    topic + "/config",
                    topic + "/device/+/edit",
                    topic + "/device/+/permission",
                    topic + "/device/+/delete",
                    topic + "/device/+/command",
                    topic + "/device/+/bind",
                    topic + "/device/+/unbind",
                    topic + "/scenario/config",
                    topic + "/scenario/+/start",
                    topic + "/scenario/+/delete",
                    topic + "/scenario/+/edit",
                    topic + "/rule/config",
                    topic + "/rule/+/delete",
                    topic + "/rule/+/edit",
                    topic + "/environment/config",
                    topic + "/environment/+/delete",
                    topic + "/environment/+/edit"
                ], { qos: 1 }
            );
        });
    }
});

mqttGatewayClient.on("message", (topic, message) => {
    try {
        console.log(chalk.cyan("----------------SUBSCRIBE---------------"));
        console.log(chalk.cyan("---Topico: " + topic));
        console.log(chalk.cyan("---Payload: "));
        console.log(chalk.cyan(message));
        console.log(chalk.cyan("----------------SUBSCRIBE---------------"));

        var correlId = JSON.parse(message).messageId;
        mqttServices.execRequest(topic, message).then(function (callback) {
            //RESULT

            var payload = {
                messageId: uuidv4(),
                correlId: correlId,
                timestamp: Date.now(),
                data: callback.data
            }
            mqttInPub(preTopic + "/success", payload);
            //console.log(chalk.green(callback));
        }).catch(function (err) {
            //REJECT

            var payload = {
                messageId: uuidv4(),
                correlId: correlId,
                timestamp: Date.now(),
                data: err
            }
            mqttInPub(preTopic + "/error", payload);
            // console.log(chalk.red("REJECT REQUEST: "));
            // console.log(chalk.red(err));    
        })
    } catch (error) {
        console.log(chalk.red("[mqtt-client] erro ao executar comando via mqtt: " + error));
    }
});

exports.mqttPub = async (topic, message) => {
    console.log(chalk.cyan("-----------------PUBLISH----------------"));
    console.log(chalk.cyan("---Topico: " + topic));
    console.log(chalk.cyan("---Payload: "));
    console.log(chalk.cyan(JSON.stringify(message)));
    console.log(chalk.cyan("-----------------PUBLISH----------------"));

    mqttGatewayClient.publish(topic, JSON.stringify(message), { qos: 1 });
};

function mqttInPub(topic, message) {
    console.log(chalk.cyan("-----------------PUBLISH----------------"));
    console.log(chalk.cyan("---Topico: " + topic));
    console.log(chalk.cyan("---Payload: "));
    console.log(chalk.cyan(JSON.stringify(message)));
    console.log(chalk.cyan("-----------------PUBLISH----------------"));

    mqttGatewayClient.publish(topic, JSON.stringify(message), { qos: 1 });
};

exports.getMqttTopic = () => {
    //console.log(shepherdTopic + "/");
    return preTopic + "/";
  }