const zsShepherd = require("../shepherd/zs-shepherd");
const chalk = require("chalk");

const deviceDAO = require("../models/DAO/device.dao");

exports.execRequest = async (devIeeeAddr, operation, nJSON) => {
    return new Promise((resolve, reject) => {
        switch (operation) {
            case "command":
                console.log(chalk.green("[mqtt-service] device/+/command"));

                nJSON.ieeeAddr = devIeeeAddr;
                zsShepherd.deviceCommand(nJSON).then(function (callback) {
                    console.log(chalk.green("[z-shepherd] functional done: "));
                    console.log(chalk.green(callback));
                    resolve(callback);
                }).catch(function (err) {
                    reject(err.toString());
                });
                break;
            case "edit":
                console.log(chalk.green("[mqtt-service] device/+/edit"));

                var data = {
                    name: nJSON.name
                }
                var device = {
                    deviceId: devIeeeAddr
                }

                deviceDAO.updateDevice(device, data).then(function (callback) {
                    console.log(chalk.green("[nedb] device editado. "));
                    resolve(callback);
                }).catch(function (err) {
                    reject(err.toString());
                });

                break;
            case "delete":
                console.log(chalk.green("[mqtt-service] device/+/delete " + devIeeeAddr));

                var device = {
                    ieeeAddr: devIeeeAddr
                }

                zsShepherd.remove(device).then(function (callback) {
                    console.log(chalk.green("[z-shepherd] device removido."));
                    deviceDAO.deleteDeviceByIeeeAddr(device).then(function (callback2) {
                        console.log(chalk.green("[nedb] device removido."));
                        resolve(device.ieeeAddr);
                    }).catch(function (err) {
                        reject(err.toString());
                    });
                }).catch(function (err) {
                    reject(err.toString());
                });
                break;
            case "bind":
                console.log(chalk.green("[mqtt-service] device/+/bind"));

                var bind = {
                    interruptor: {
                        ieeeAddr: nJSON.interruptor.ieeeAddr,
                        epId: nJSON.interruptor.epId,
                        cId: ""
                    },
                    saidas: nJSON.saida
                }

                nJSON.saida.forEach(saida => {
                    bind.interruptor.cId = "genOnOff";
                    zsShepherd.newBind(bind.interruptor, saida).then(function (callback) {
                        bind.interruptor.cId = "genLevelCtrl";
                        zsShepherd.newBind(bind.interruptor, saida).then(function (callback2) {
                            resolve({
                                bind1: callback,
                                bind2: callback2
                            });
                        }).catch(function (err) {
                            reject(err.toString());
                        });
                    }).catch(function (err) {
                        reject(err.toString());
                    });
                })
                break;
            case "unbind":
                console.log(chalk.green("[mqtt-service] device/+/unbind"));

                var bind = {
                    interruptor: {
                        ieeeAddr: nJSON.interruptor.ieeeAddr,
                        epId: nJSON.interruptor.epId,
                        cId: ""
                    },
                    saidas: nJSON.saida
                }

                nJSON.saida.forEach(saida => {
                    bind.interruptor.cId = "genOnOff";
                    zsShepherd.unbind(bind.interruptor, saida).then(function (callback) {
                        bind.interruptor.cId = "genLevelCtrl";
                        zsShepherd.unbind(bind.interruptor, saida).then(function (callback2) {
                            resolve({
                                bind1: callback,
                                bind2: callback2
                            });
                        }).catch(function (err) {
                            reject(err.toString());
                        });
                    }).catch(function (err) {
                        reject(err.toString());
                    });
                });
                break;
        }
    });
}