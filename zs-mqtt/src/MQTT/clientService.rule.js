const zsShepherd = require("../shepherd/zs-shepherd");
const chalk = require("chalk");

const ruleDAO = require("../models/DAO/rule.dao");

exports.execRequest = async (ruleId, operation, nJSON) => {
    return new Promise((resolve, reject) => {
        if (!operation) {
            console.log(chalk.green("[mqtt-service] rule/config"));

            var data = {
                ruleId: nJSON.ruleId,
                name: nJSON.name,
                inputs: nJSON.inputs,
                outputs: nJSON.outputs,
                schedule: nJSON.schedule,
                isActive: nJSON.isActive
            }
            ruleDAO.createRule(data).then(function (rule) {
                console.log(chalk.green("[nedb] regra adicionada."));
                resolve(rule);
            }).catch(function (err) {
                reject(err.toString());
            });
        } else {
            switch (operation) {
                case "edit":
                    console.log(chalk.green("[mqtt-service] rule/+/edit"));

                    var data = {
                        ruleId: ruleId,
                        name: nJSON.name,
                        inputs: nJSON.inputs,
                        outputs: nJSON.outputs,
                        schedule: nJSON.schedule,
                        isActive: nJSON.isActive
                    }
                    ruleDAO.updateRule(ruleId, data).then(function (rule) {
                        console.log(chalk.green("[nedb] regra editada."));
                        resolve(rule);
                    }).catch(function (err) {
                        reject(err.toString());
                    });
                    break;
                case "delete":
                    console.log(chalk.green("[mqtt-service] rule/+/delete"));

                    ruleDAO.deleteRule(ruleId).then(function (callback) {
                        console.log(chalk.green("[nedb] regra removida."));
                        resolve(callback);
                    }).catch(function (err) {
                        reject(err.toString());
                    });
                    break;
            }
        }
    });
}