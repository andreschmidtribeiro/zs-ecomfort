const zsShepherd = require("../shepherd/zs-shepherd");
const chalk = require("chalk");

const deviceService = require("./clientService.device");
const environmentService = require("./clientService.environment");
const scenarioService = require("./clientService.scenario");
const ruleService = require("./clientService.rule");
const gatewayService = require("./clientService.gateway");

const uuidv4 = require('uuid/v4');

exports.execRequest = async (topic, message) => {
    return new Promise((resolve, reject) => {
        try {
            var nJSON = JSON.parse(message);
            var splitedTopic = topic.split("/");
            var preTopic = splitedTopic[0] + "/" + splitedTopic[1] + "/" + splitedTopic[2] + "/" + splitedTopic[3] + "/" + splitedTopic[4] + "/" + splitedTopic[5] + "/";

            switch (splitedTopic[6]) {
                case "config":
                    console.log(chalk.green("[mqtt-service] gateway config"));

                    gatewayService.execRequest(nJSON).then(function (callback) {
                        resolve({
                            topic: preTopic + "success",
                            data: callback
                        });
                    }).catch(function (err) {
                        reject(err.toString())
                    })
                    break;
                case "reset":
                    console.log(chalk.green("[mqtt-service] reset gateway " + nJSON.mode));

                    zsShepherd.listAll().then(function (callback) {
                        console.log(chalk.green("[z-shepherd] reset successfully."));
                        resolve({
                            topic: preTopic + "success",
                            data: callback
                        });
                    }).catch(function (err) {
                        reject(err.toString());
                    });
                    break;
                case "addDevice":
                    console.log(chalk.green("[mqtt-service] addDevice"));

                    zsShepherd.addDevice(nJSON.time).then(function (callback) {
                        console.log(chalk.green("[z-shepherd] permit join enabled: " + callback + " seconds"));
                        resolve({
                            topic: preTopic + "success",
                            data: {
                                time: callback
                            }
                        });
                    }).catch(function (err) {
                        reject(err.toString());
                    });
                    break;
                case "device":
                    deviceService.execRequest(splitedTopic[7], splitedTopic[8], nJSON).then(function (callback) {
                        resolve({
                            topic: preTopic + "success",
                            data: callback
                        });
                    }).catch(function (err) {
                        reject(err.toString())
                    })
                    break;
                case "environment":
                    environmentService.execRequest(splitedTopic[7], splitedTopic[8], nJSON).then(function (callback) {
                        resolve({
                            topic: preTopic + "success",
                            data: callback
                        });
                    }).catch(function (err) {
                        reject(err.toString())
                    })
                    break;
                case "scenario":
                    scenarioService.execRequest(splitedTopic[7], splitedTopic[8], nJSON).then(function (callback) {
                        resolve({
                            topic: preTopic + "success",
                            data: callback
                        });
                    }).catch(function (err) {
                        reject(err.toString())
                    })
                    break;
                case "rule":
                    ruleService.execRequest(splitedTopic[7], splitedTopic[8], nJSON).then(function (callback) {
                        resolve({
                            topic: preTopic + "success",
                            data: callback
                        });
                    }).catch(function (err) {
                        reject(err.toString())
                    })
                    break;
                case "list":
                    console.log(chalk.green("[mqtt-service] device list"));

                    zsShepherd.listAll().then(function (callback) {
                        console.log(chalk.green("[z-shepherd] device list: "));
                        resolve({
                            topic: preTopic + "success",
                            data: callback
                        });
                    }).catch(function (err) {
                        reject(err.toString());
                    });
                    break;
                default:
                    console.log(chalk.red("-----------------Comando 404----------------"));
                    console.log(chalk.red("---Topico: " + topic));
                    console.log(chalk.red("---Data: "));
                    console.log(chalk.red(message));
                    console.log(chalk.red("-----------------Comando 404----------------"));
                    break;
            }
        } catch (err) {
            reject(err);
        }
    });
}