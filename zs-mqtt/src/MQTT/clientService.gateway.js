const zsShepherd = require("../shepherd/zs-shepherd");
const chalk = require("chalk");

const gatewayDAO = require("../models/DAO/gateway.dao");

exports.execRequest = async (nJSON) => {
    return new Promise((resolve, reject) => {
        gatewayDAO.finGateway(nJSON).then(function (gateway) {
            console.log(chalk.green("[nedb] gateway: " + gateway._id));
            resolve({
                name: gateway.name,
                ipv4: gateway.ipv4,
                serialNumber: gateway.serialNumber,
                gatewayId: gateway._id
            });
        }).catch(function (err) {
            reject(err.toString());
        });
    });
}