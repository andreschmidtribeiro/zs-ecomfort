const zsShepherd = require("../shepherd/zs-shepherd");
const chalk = require("chalk");

const scenarioDAO = require("../models/DAO/scenario.dao");

exports.execRequest = async (scenarioId, operation, nJSON) => {
    return new Promise((resolve, reject) => {
        if (!operation) {
            console.log(chalk.green("[mqtt-service] scenario/config"));

            var data = {
                scenarioId: nJSON.scenarioId,
                name: nJSON.name,
                actions: nJSON.actions
            }
            scenarioDAO.createScenario(data).then(function (scenario) {
                console.log(chalk.green("[nedb] scenario adicionado."));
                resolve(scenario);
            }).catch(function (err) {
                reject(err.toString());
            });
        } else {
            switch (operation) {
                case "edit":
                    console.log(chalk.green("[mqtt-service] scenario/edit"));

                    var data = {
                        scenarioId: scenarioId,
                        name: nJSON.name,
                        actions: nJSON.actions
                    }
                    scenarioDAO.updateScenario(scenarioId, data).then(function (scenario) {
                        console.log(chalk.green("[nedb] scenario editado."));
                        resolve(scenario);
                    }).catch(function (err) {
                        reject(err.toString());
                    });
                    break;
                case "delete":
                    console.log(chalk.green("[mqtt-service] scenario/delete"));

                    scenarioDAO.deleteScenario(scenarioId).then(function () {
                        console.log(chalk.green("[nedb] scenario removido."));
                        resolve("");
                    }).catch(function (err) {
                        reject(err.toString());
                    });
                    break;
                case "start":
                    console.log(chalk.green("[mqtt-service] scenario/start"));

                    scenarioDAO.listScenarioById(scenarioId).then(function (scenario) {
                        scenario.actions.forEach(functional => {
                            zsShepherd.deviceCommand(functional);
                        });
                        resolve(scenario);
                    }).catch(function (err) {
                        reject(err.toString());
                    });
                    break;
            }
        }
    });
}
