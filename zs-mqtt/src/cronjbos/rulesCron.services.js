var CronJob = require("cron").CronJob;

var jobs = [];

exports.jobs;

exports.job = async (rule, schedule) => {
  await jobs.push(new CronJob(
    schedule,
    function() {
      rule.inputs.forEach(input => {
        console.log("Este é um input!");
      });
      rule.outputs.forEach(output => {
        console.log("Este é um output!");
      });
      /*
   * Runs every weekday (Monday through Friday)
   * at 11:30:00 AM. It does not run on Saturday
   * or Sunday.
   */
    },
    function() {
      console.log("Acabou a regra =D");
      /* This function is executed when the job stops */
    },
    true, /* Start the job right now */
    Intl.DateTimeFormat().resolvedOptions().timeZone /* Time zone of this job. */
  ));
};

exports.stopAll = () => {
  jobs.forEach(rule => {
    rule.stop();
  });
}

exports.startAll = () => {
  jobs.forEach(rule => {
    rule.start();
  });
}

exports.applyPolitics = () => {
  jobs.forEach(rule => {
    if(rule.isActive) {
      rule.start();
    } else {
      rule.stop();
    }
  });
}



// schedule = '* * * * * *'
// Seconds: 0-59
// Minutes: 0-59
// Hours: 0-23
// Day of Month: 1-31
// Months: 0-11
// Day of Week: 0-6
//
// var CronJob = require('cron').CronJob;
// var job = new CronJob('00 30 11 * * 1-5', function() {
//   /*
//    * Runs every weekday (Monday through Friday)
//    * at 11:30:00 AM. It does not run on Saturday
//    * or Sunday.
//    */
//   }, function () {
//     /* This function is executed when the job stops */
//   },
//   true, /* Start the job right now */
//   timeZone /* Time zone of this job. */
// );
