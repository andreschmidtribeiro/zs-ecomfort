const Rules = require("../models/rule.model");
const zsShepherd = require("../shepherd/zs-shepherd");
const moment = require("moment");
const _ = require("lodash");
const chalk = require("chalk");

//const mqtt = require("../MQTT/z-shepherd/z-shepherd-topic.mqtt");
const uuidv4 = require('uuid/v4');

exports.testIfRuleInput = async message => {
  let msg = {
    ieeeAddr: message.ieeeAddr + "",
    type: message.type,
    value: message.value,
    sensorType: message.sensorType,
    epId: message.epId
  };

  let rules = await Rules.find();
  rules.forEach(rule => {
    //console.log(chalk.blue("[rule-processor] nome: " + rule.name));
    if (rule.isActive && itsTime(rule.schedule)) {
      rule.inputs.forEach(input => {
        if (
          JSON.stringify(input.type) == JSON.stringify(msg.type) &&
          JSON.stringify(input.ieeeAddr) == JSON.stringify(msg.ieeeAddr)
        ) {
          console.log(chalk.blue("[rule-processor] type: " + JSON.stringify(msg.type) + " = " + JSON.stringify(input.type)));
          console.log(chalk.blue("[rule-processor] ieeeAddr: " + JSON.stringify(msg.ieeeAddr) + " = " + JSON.stringify(input.ieeeAddr)));
          if (input.condition === "=") {
            console.log(chalk.blue("[rule-processor] value: " + JSON.stringify(msg.value) + " = " + JSON.stringify(input.value)));
            if (JSON.stringify(msg.value) == JSON.stringify(input.value)) {
              executeOutputs(rule.outputs, rule.ruleId, rule.name);
            }
          } else if (input.condition === ">") {
            console.log(chalk.blue("[rule-processor] value: " + JSON.stringify(msg.value) + " > " + JSON.stringify(input.value)));
            if (JSON.stringify(msg.value) > JSON.stringify(input.value)) {
              executeOutputs(rule.outputs, rule.ruleId, rule.name);
            }
          } else if (input.condition === "<") {
            console.log(chalk.blue("[rule-processor] value: " + JSON.stringify(msg.value) + " < " + JSON.stringify(input.value)));
            if (JSON.stringify(msg.value) < JSON.stringify(input.value)) {
              executeOutputs(rule.outputs, rule.ruleId, rule.name);
            }
          }
        }
      });
    }
  });
};

itsTime = schedule => {
  let startDay = schedule.interval.startDay ? schedule.interval.startDay : null;
  let endDay = schedule.interval.endDay ? schedule.interval.endDay : null;
  let startTime = schedule.interval.startHour ? schedule.interval.startHour : null;
  let endTime = schedule.interval.endHour ? schedule.interval.endHour : null;
  let weekdays = schedule.weekdays ? schedule.weekdays : null;

  let startDate = startDay + " " + startTime;
  let endDate = endDay + " " + endTime;

  if (startDate != "0 0" && endDate != "0 0") {
    if (moment().isBetween(startDate, endDate)) {
      if (weekdays.length > 0) {
        // console.log("INTERVALO COM DIA DA SEMANA");
        let isWeekDay = false;
        weekdays.forEach(weekday => {
          if (weekday.day === moment().weekday()) {
            isWeekDay = true;
          }
        });
        if (isWeekDay) {
          // console.log("É DIA!");
          return true;
        } else {
          // console.log("NÃO É DIA!");
          return false;
        }
      } else {
        // console.log("SOMENTE INTERVALO");
        return true;
      }
    } else if (!moment().isBetween(startDate, endDate)) {
      // console.log("INTERVALO INVÁLIDO");
      return false;
    }
  } else if (weekdays.length > 0) {
    // console.log("SOMENTE DIA DA SEMANA");
    let isWeekDay = false;
    weekdays.forEach(weekday => {
      if (weekday.day === moment().weekday()) {
        isWeekDay = true;
      }
    });
    if (isWeekDay) {
      // console.log("É DIA!");
      return true;
    } else {
      // console.log("NÃO É DIA!");
      return false;
    }
  } else {
    // console.log("PRA SEMPRE");
    return true;
  }
};

executeOutputs = (outputs, ruleId, ruleName) => {
  outputs.forEach(output => {
    zsShepherd.deviceCommand(output).then(function (callback) {
      console.log(chalk.green("[z-shepherd] Functional done: " + callback));
    }).catch(function (err) {
      console.log(chalk.red("[rule-processor] Functional " + err));
    });
  })

  //sendMqtt("rule/" + ruleId + "/status", ruleName);
  console.log(chalk.green("[rule-processor] Regra: " + ruleId + ", aconteceu!"));

};


// const sendMqtt = async (topic, message) => {
//   topic = await mqtt.getMqttTopic() + topic;
//   var payload = {
//     messageID : uuidv4(),
//     timestamp : Date.now(),
//     name : message
//   }
//   mqtt.mqttPub(topic, payload);
// };