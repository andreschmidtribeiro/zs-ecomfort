const connect = require("camo").connect;
const network = require("network");
const chalk = require("chalk");

const gatewayDAO = require("./src/models/DAO/gateway.dao");
const Gateway = require("./src/models/gateway.model");

var database;
const uri = "nedb://.././database/";

connect(uri).then(function(db) {
  database = db;
});

const shepherd = require('./src/shepherd/zs-shepherd');


network.get_active_interface(function (err, obj) {
  console.log(chalk.blue("[network] ipv4: {"));
  console.log(chalk.blue("[network]   address: " + obj.ip_address));
  console.log(chalk.blue("[network]   mask: " + obj.netmask));
  console.log(chalk.blue("[network]   gateway: " + obj.gateway_ip));
  console.log(chalk.blue("[network] }"));

  var data = {
    ipv4: {
      address: obj.ip_address,
      mask: obj.netmask,
      gateway: obj.gateway_ip
    }
  }
  Gateway.findOne({ status: "active" }).then(function (gateway) {
    console.log(gateway._id);
    gatewayDAO.updateGateway(gateway._id, data).then(function (callback) {
      console.log(chalk.blue("[nedb] gateway ip atualizado."));
    });
  });
});